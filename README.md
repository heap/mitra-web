# Mitra Web

Default frontend for [Mitra](https://codeberg.org/silverpill/mitra).

<img width="600" src="screenshot.png" alt="screenshot">

## Requirements

- node 12 or 14
- npm 7+

## Project setup

```
npm install
npx allow-scripts
```

### Compiles and hot-reloads for development

```
npm start
```

### Compile and minify for production

```
echo "VUE_APP_BACKEND_URL=https://mydomain.tld" > .env.local
npm run build
```

### Run your unit tests

```
npm run test
```

### Lint files

```
npm run lint
```
